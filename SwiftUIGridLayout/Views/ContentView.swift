////  ContentView.swift
//  SwiftUIGridLayout
//
//  Created on 13/11/2020.
//  
//

import SwiftUI

struct ContentView: View {
    

    let helloweens = ["1", "2", "3", "4", "5", "6","7","8","9" ]
    @State private var sliderValue: CGFloat = 1

    var body: some View {
        
        NavigationView {
            
            VStack {
                
                Slider(value: $sliderValue, in: 1...9, step: 1)
                Text(String(format: "%.0f", sliderValue))
                    .font(.system(size: 18))
                    .fontWeight(.bold)
                    .padding()
                    .background(Color.red)
                    .foregroundColor(.white)
                    .clipShape(Circle())
                
                List(self.helloweens.chunks(size: Int(sliderValue)), id: \.self) { chunk in
                    ForEach(chunk, id: \.self) { hello in
                        Image(hello)
                            .resizable()
                            .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fit/*@END_MENU_TOKEN@*/)
                    }
                }
            }
            
        .navigationBarTitle("Helloween")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
